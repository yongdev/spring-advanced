package com.example.advanced.trace;

public class TraceStatus {

    private TraceId traceId;
    private Long startTimeMs; //로그의 시작과 끝을 확인하기 위함
    private String message;

    public TraceStatus(TraceId traceId, Long startTimeMs, String message) {
        this.traceId = traceId;
        this.startTimeMs = startTimeMs;
        this.message = message;
    }
    public Long getStartTimeMs() {
        return startTimeMs;
    }
    public String getMessage() {
        return message;
    }
    public TraceId getTraceId() {
        return traceId;
    }

}
